import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import math
from tensorflow.keras.layers import Input, Dense, Dropout, Concatenate
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K


def dataGen(X):
    #Y = np.sin(X * math.pi)*0.5
    #noise = np.where(X > 0, X*np.random.random(X.shape), 0)*0.2
    #return Y + noise
    Y = np.exp(-X**2)*0.5
    noise = np.where(X > 2, (X-2)*np.random.random(X.shape), 0)*0.2
    return Y + noise


Xtrain = np.random.rand(10000)*6-2
Ytrain = dataGen(Xtrain)

Xtest = np.linspace(-4,4,1000)
Ytest = dataGen(Xtest)

#plt.scatter(X,Y)
#plt.show()

inputs = Input(shape=(1,))

output_1 = Dense(64, activation='tanh')(inputs)
#drop_1 = Dropout(output_1)
output_2 = Dense(64, activation='tanh')(output_1)
#drop_2 = Dropout(output_2)
predictions = Dense(1)(output_2)
logvar = Dense(1)(output_2)

out_concat = Concatenate()([predictions, logvar])

# Define custom loss
def custom_loss(y_true,y_pred):
    s = y_pred[:,1]
    y_true = y_true[:,0]
    y_pred = y_pred[:,0]
    L = K.mean(K.square(y_pred - y_true) * K.exp(-s) + s, axis=-1)
    return L
   

varmodel = Model(inputs=inputs, outputs=logvar)
model = Model(inputs=inputs, outputs=out_concat)
model.compile(optimizer='adam', loss=custom_loss)
model.summary()
Yfit = np.array([Ytrain, np.zeros(shape=Ytrain.shape)]).T
for n in range(20):
    model.fit(Xtrain, Yfit, epochs=10) 

    #logvarpred = varmodel.predict(Xtest)
    Ypred = model.predict(Xtest)

    ax = plt.gca()
    plt.scatter(Xtrain[:1000], Ytrain[:1000], label="train set")
    Ys = Ypred[:,1]
    Ysigma = np.sqrt(np.exp(Ys))
    ax.fill_between(Xtest, Ypred[:,0]+Ysigma, Ypred[:,0]-Ysigma, facecolor='green', alpha=0.5)
    plt.plot(Xtest, Ypred[:,0], label="prediction", color="green", zorder=10)
    plt.legend()
    plt.savefig("plots/png/aleatoric-uncertainty-{}.png".format(n+1), dpi=160)
    plt.savefig("plots/pdf/aleatoric-uncertainty-{}.pdf".format(n+1), dpi=160)
    plt.clf()