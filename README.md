# Uncertainty Estimation in Deep Learning

Recently I've come accross a publication by [Kendall and Gal](https://arxiv.org/abs/1703.04977) "What Uncertainties Do We Need in Bayesian Deep Learning for Computer Vision?"

In their paper the authors tackle the inherent problem of deep learning approaches to be overconfident.
Given an input X, a deep learning regression or classification model will predict a corresponding output Y.
In many cases this prediction may be correct, however the model has no means of predicting it's own prediction accuracy.

The approach by Kendall and Gal introduces two methods to incorporate uncertainty estimation into the model.
The first uncertainty component, called epistemic uncertainty measures the model-inherent uncertainty.
The epistemic uncertainty relates to the confidence of the model to make a correct prediction.
For example, this uncertainty is high when the model is asked to make a prediction for a data-poin, that is far away from anything the model has been trained on.

The second uncertainty component, called aleatoric uncertainty measures the variance in the data.
The aleatoric uncertainty relates to inconsistencies in the training data.
For example, if for one input value X multiple varying output values Y are trained then the aleatoric uncertainty is high.


# About this Project

This project replicates the findings of Kendall and Gal using a small regression example, that can be trained on the CPU.
The data is synthetic. There are three scrips:


| Script | Description |
| ------ | ------ |
| epistemic-uncertainty.py | Estimate epistemic uncertainty |
| aleatoric-uncertainty.py | Estimate aleatoric uncertainty |
| combined-uncertainty.py  | Estimate both uncertainties    |


## Epistemic Uncertainty

The model can predict, that it's uncertain about values for x < -2, however the variance in the data for x > 2 is not captured.
![alt text](plots/png/epistemic-uncertainty-20.png)

## Aleatoric Uncertainty

The model can predict the variance in thedata for x > 2, however the model is over-confident for predictions for x < -2.
![alt text](plots/png/aleatoric-uncertainty-20.png)

## Both Uncertainties

Variance in the data and model uncertainty can be predicted at the same time.
![alt text](plots/png/combined-uncertainty-20.png)
