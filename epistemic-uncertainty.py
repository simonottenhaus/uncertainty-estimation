import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import math
from tensorflow.keras.layers import Input, Dense, Dropout, Concatenate
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K


def dataGen(X):
    Y = np.exp(-X**2)*0.5
    noise = np.where(X > 2, (X-2)*np.random.random(X.shape), 0)*0.2
    return Y + noise


Xtrain = np.random.rand(10000)*6-2
Ytrain = dataGen(Xtrain)

Xtest = np.linspace(-4,4,1000)
Ytest = dataGen(Xtest)

#plt.scatter(X,Y)
#plt.show()

inputs = Input(shape=(1,))

output_1 = Dense(64, activation='relu')(inputs)
drop_1 = Dropout(0.5)(output_1, training = True)
output_2 = Dense(64, activation='relu')(drop_1)
drop_2 = Dropout(0.5)(output_2, training = True)
predictions = Dense(1)(drop_2)



model = Model(inputs=inputs, outputs=predictions)
model.compile(optimizer='adam', loss='mse')
model.summary()

for n in range(20):
    model.fit(Xtrain, Ytrain, epochs=10) 
    model.save("checkpoints/epistemic-uncertainty-{}.h5".format(n+1))

    #logvarpred = varmodel.predict(Xtest)
    predictions = []
    for i in range(0,50):
        Ypred = model.predict(Xtest)
        predictions.append(Ypred)

    predictions = np.array(predictions)
    YpredMean = np.mean(predictions, axis=0)
    YpredVar  = np.std(predictions, axis=0)
    YpredMean=YpredMean[:,0]
    YpredVar=YpredVar[:,0]
    #print(YpredMean.shape)
    #print(YpredVar.shape)

    ax = plt.gca()
    plt.scatter(Xtrain[:1000], Ytrain[:1000], label="train set")
    ax.fill_between(Xtest, YpredMean+YpredVar, YpredMean-YpredVar, facecolor='orange', alpha=0.5)
    plt.plot(Xtest, YpredMean, label="mean", color="orange")
    plt.legend()
    plt.savefig("plots/png/epistemic-uncertainty-{}.png".format(n+1), dpi=160)
    plt.savefig("plots/pdf/epistemic-uncertainty-{}.pdf".format(n+1), dpi=160)
    plt.clf()