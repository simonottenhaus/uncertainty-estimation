import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import math
from tensorflow.keras.layers import Input, Dense, Dropout, Concatenate
from tensorflow.keras.models import Model
import tensorflow.keras.backend as K


def dataGen(X):
    #Y = np.sin(X * math.pi)*0.5
    #noise = np.where(X > 0, X*np.random.random(X.shape), 0)*0.2
    #return Y + noise
    Y = np.exp(-X**2)*0.5
    noise = np.where(X > 2, (X-2)*np.random.random(X.shape), 0)*0.2
    return Y + noise


Xtrain = np.random.rand(10000)*6-2
Ytrain = dataGen(Xtrain)

Xtest = np.linspace(-4,4,1000)
Ytest = dataGen(Xtest)

#plt.scatter(X,Y)
#plt.show()

inputs = Input(shape=(1,))

layer = Dense(64, activation='relu')(inputs)
layer = Dropout(0.3)(layer, training = True)
layer = Dense(64, activation='relu')(layer)
layer = Dropout(0.3)(layer, training = True)
layer = Dense(64, activation='relu')(layer)
layer = Dropout(0.3)(layer, training = True)
predictions = Dense(1)(layer)
logvar = Dense(1)(layer)

out_concat = Concatenate()([predictions, logvar])

# Define custom loss
def custom_loss(y_true,y_pred):
    s = y_pred[:,1]
    y_true = y_true[:,0]
    y_pred = y_pred[:,0]
    L = K.mean(K.square(y_pred - y_true) * K.exp(-s) + s, axis=-1)
    return L
   

varmodel = Model(inputs=inputs, outputs=logvar)
model = Model(inputs=inputs, outputs=out_concat)
model.compile(optimizer='adam', loss=custom_loss)
model.summary()
Yfit = np.array([Ytrain, np.zeros(shape=Ytrain.shape)]).T
for n in range(20):
    model.fit(Xtrain, Yfit, epochs=10) 

    predictions = []
    for i in range(0,50):
        Ypred = model.predict(Xtest)
        predictions.append(Ypred)

    predictions = np.array(predictions)
    YpredMean = np.mean(predictions, axis=0)
    YpredVar  = np.std(predictions, axis=0)
    Ys        = YpredMean[:,1]
    YpredMean = YpredMean[:,0]
    YpredVar  = YpredVar[:,0]


    ax = plt.gca()
    plt.scatter(Xtrain[:1000], Ytrain[:1000], label="train set")
    #Ys = Ypred[:,1]
    Ysigma = np.sqrt(np.exp(Ys))
    ax.fill_between(Xtest, YpredMean+YpredVar, YpredMean-YpredVar, facecolor='orange', alpha=0.5)
    ax.fill_between(Xtest, YpredMean+YpredVar, YpredMean+YpredVar+Ysigma, facecolor='green', alpha=0.5)
    ax.fill_between(Xtest, YpredMean-YpredVar, YpredMean-YpredVar-Ysigma, facecolor='green', alpha=0.5)

    plt.plot(Xtest, YpredMean, label="prediction", color="orange", zorder=10)
    plt.legend()
    plt.savefig("plots/png/combined-uncertainty-{}.png".format(n+1), dpi=160)
    plt.savefig("plots/pdf/combined-uncertainty-{}.pdf".format(n+1), dpi=160)
    plt.clf()